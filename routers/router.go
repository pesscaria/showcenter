package routers

import (
	"showcenter/controllers"

	"github.com/astaxie/beego"
)

func init() {

	showcenter := beego.NewNamespace("/showcenter",
		beego.NSRouter("/", &controllers.MainController{}),

		beego.NSNamespace("/node",
			beego.NSRouter("/show", &controllers.NodeController{}, "get:ShowNodes"),
			beego.NSRouter("/style/show", &controllers.NodeController{}, "get:ShowStyle"),
		),

		// 约操作路由
		beego.NSNamespace("/dating",
			beego.NSRouter("/home", &controllers.DatingController{}, "*:HomePage"),
			beego.NSRouter("/new", &controllers.DatingController{}, "post:NewDating"),                                  //新建约会
			beego.NSRouter("/:articleID([0-9a-f]{24})/edit", &controllers.DatingController{}, "post:EditDating"),       //编辑约会
			beego.NSRouter("/:articleID([0-9a-f]{24})/delete", &controllers.DatingController{}, "delete:DeleteDating"), //删除约会
			beego.NSRouter("/:articleID([0-9a-f]{24})/join", &controllers.DatingController{}, "post:JoinDating"),       //加入约会申请
			beego.NSRouter("/:articleID([0-9a-f]{24})/agree", &controllers.DatingController{}, "post:AgreeDating"),     //同意申请
			//beego.NSRouter("/:articleID([0-9a-f]{24})/show", &controllers.DatingController{}, "get:ShowDating"),      //显示约会
			beego.NSRouter("/query/style", &controllers.DatingController{}, "post:ShowWithStyle"),
			beego.NSRouter("/query/startdate", &controllers.DatingController{}, "post:ShowWithStartDate"),
			beego.NSRouter("/query/destination", &controllers.DatingController{}, "post:ShowWithDestination"),
		),

		// 玩操作路由
		beego.NSNamespace("/tour",
			beego.NSRouter("/home", &controllers.TourController{}, "*:HomePage"),
			beego.NSRouter("/new", &controllers.TourController{}, "post:NewTour"),                                  //新建游玩
			beego.NSRouter("/:articleID([0-9a-f]{24})/edit", &controllers.TourController{}, "get:EditTour"),        //编辑游玩
			beego.NSRouter("/:articleID([0-9a-f]{24})/delete", &controllers.TourController{}, "delete:DeleteTour"), //删除游玩
			//beego.NSRouter("/:articleID([0-9a-f]{24})/show", &controllers.TourController{}, "get:ShowTour"),        //显示游玩
			beego.NSRouter("/query/style", &controllers.TourController{}, "post:ShowWithStyle"),
			beego.NSRouter("/query/startdate", &controllers.TourController{}, "post:ShowWithStartDate"),
			beego.NSRouter("/query/destination", &controllers.TourController{}, "post:ShowWithDestination"),
		),

		//评论操作路由
		beego.NSNamespace("/comment",
			beego.NSRouter("/:articleID([0-9a-f]{24})/new", &controllers.CommentController{}, "post:NewComment"),          //新建评论
			beego.NSRouter("/:articleID([0-9a-f]{24})/show", &controllers.CommentController{}, "post:ShowComment"),        //展示文章评论
			beego.NSRouter("/:commentID([0-9a-f]{24})/delelte", &controllers.CommentController{}, "delete:DeleteComment"), //删除评论
			beego.NSRouter("/:commentID([0-9a-f]{24})/edit", &controllers.CommentController{}, "post:EditComment"),        //编辑评论
			beego.NSRouter("/:commentID([0-9a-f]{24})/reply", &controllers.CommentController{}, "post:ReplyComment"),
			beego.NSRouter("/:commentID([0-9a-f]{24})/like", &controllers.CommentController{}, "post:LikeComment"),
		),

		// 赞 收藏 分享 等
		beego.NSNamespace("/option",
			beego.NSRouter("/:articleID([0-9a-f]{24})/like", &controllers.OptionController{}, "post:Like"),
			beego.NSRouter("/:articleID([0-9a-f]{24})/unlike", &controllers.OptionController{}, "post:UnLike"),
			beego.NSRouter("/:articleID([0-9a-f]{24})/share", &controllers.OptionController{}, "post:Share"),
			beego.NSRouter("/:articleID([0-9a-f]{24})/collect", &controllers.OptionController{}, "post:Collect"),
			beego.NSRouter("/:articleID([0-9a-f]{24})/uncollect", &controllers.OptionController{}, "post:UnCollect"),
			beego.NSRouter("/:articleID([0-9a-f]{24})/hit", &controllers.OptionController{}, "*post:Hit"),
		),

		// 个人信息
		beego.NSNamespace("/personal",
			beego.NSRouter("/comments", &controllers.PersonalController{}, "get:ListComments"),
			beego.NSRouter("/collections", &controllers.PersonalController{}, "get:ListCollections"),
			beego.NSRouter("/mine", &controllers.PersonalController{}, "get:ListMine"),
		),
	)
	beego.AddNamespace(showcenter)
}
