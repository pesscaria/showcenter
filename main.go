package main

import (
	_ "showcenter/routers"
	"utils"

	"github.com/astaxie/beego"
)

func main() {
	utils.InitRPCClient(map[int]string{
		utils.USER:    beego.AppConfig.String("rpc_usercenter"),
		utils.MESSAGE: beego.AppConfig.String("rpc_messagecenter"),
	})
	utils.MQInit(beego.AppConfig.String("nats"))
	beego.Run()
}
