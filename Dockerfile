# This dockerfile uses the golang image

# Base image to use, this must be set as the first line
FROM golang:latest

# Maintainer: docker_user <docker_user at email.com> (@docker_user)
MAINTAINER lunarhalo holly_loo@163.com

# Commands to update the image
ADD utils /go/src/utils

ADD showcenter /go/src/showcenter



RUN go get github.com/astaxie/beego && go get github.com/beego/bee && go get gopkg.in/mgo.v2

EXPOSE 8080

CMD ["bee", "run"]
