package controllers

import (
	"showcenter/models"
	"utils"
	"utils/proto/usercenter"

	"encoding/json"

	"github.com/astaxie/beego"
	"golang.org/x/net/context"
	"gopkg.in/mgo.v2"
)

// 账户身份状态
const (
	Traveler      = 0      //访客             0000
	Authenticated = 1 << 0 //已登陆           0001
	Administrator = 1 << 1 //管理员           0010
	NormalAccount = 1 << 2 //普通账户         0100
	TestAccount   = 1 << 3 //测试账户         1000

)

//BaseController 基础
type BaseController struct {
	beego.Controller
	*mgo.Session
	DB   *mgo.Database
	User *utils.UserProfile
	Type uint //当前会话权限
}

// Prepare 重写 Prepare
func (c *BaseController) Prepare() {
	models.LogRequest(c.Ctx.Request)
	var hwid string
	/*
		hwid := c.Ctx.Input.Header("hwid")
		if hwid == "" {
			models.LOGGER.Error("param invilid,reason: hwid is empty")
			c.Data["json"] = utils.FailReply(utils.CodeErrInvalidArgs, "硬件id为空", "报文头缺少硬件id")
			c.ServeJSON()
			return
		}
	*/
	userid := c.Ctx.Input.Header("uid")
	session := models.MongoPool.Get()
	db := session.DB(beego.AppConfig.String("mgo_database"))
	c.Session = session
	c.DB = db
	//通过rpc获取用户信息,初始会话权限
	metadata, err := utils.UserClient.QueryUserProfile(context.Background(), &usercenter.UserProfileReq{
		HwID: hwid,
	})
	if err != nil {
		models.LOGGER.Debug("Test")
		c.User = &utils.UserProfile{
			UserID: userid,
		}
		c.Type = Traveler
	} else {
		if metadata.Profile == nil {
			models.LOGGER.Debug("Traveler")
			c.User = &utils.UserProfile{
				UserID: userid,
			}
			c.Type = Traveler
		} else {
			var profile utils.UserProfile
			if err := json.Unmarshal(metadata.Profile, &profile); err != nil {
				models.LOGGER.Error("Unmarshal error, reason: %v", err)
				c.ServeJSON()
				return
			}
			if profile.IsSuperuser {
				c.Type = Authenticated | Administrator
			} else if profile.IsTest {
				c.Type = Authenticated | TestAccount
			} else {
				c.Type = Authenticated | NormalAccount
			}
			c.User = &profile
		}
	}
	models.LOGGER.Debug("prepare pass, userID:[%s]", c.User.UserID)
}

// OKJson 成功json回复
func (c *BaseController) OKJson(data interface{}) {
	c.Data["json"] = utils.OkReply(data)
	c.ServeJSON()
	c.Session.Close()
}

// FailJson 失败json回复
func (c *BaseController) FailJson(code int, err, desc string) {
	c.Data["json"] = utils.FailReply(code, err, desc)
	c.ServeJSON()
	c.Session.Close()
}
