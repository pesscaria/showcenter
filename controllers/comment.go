package controllers

import (
	"showcenter/models"
	"utils"

	"encoding/json"
)

// CommentController 评论相关Handler
type CommentController struct {
	BaseController
}

// NewComment 评论
func (c *CommentController) NewComment() {
	articleID := c.Ctx.Input.Param(":articleID")
	body := c.Ctx.Input.RequestBody
	var comment models.Comment
	if err := json.Unmarshal(body, &comment); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, err.Error(), "")
		return
	}
	if err := models.NewCommnet(c.DB, &comment, articleID, c.User.UserID); err != nil {
		models.LOGGER.Error("db error, reason: %v", err)
		c.FailJson(utils.CodeErrDb, err.Error(), "")
		return
	}
	c.OKJson(nil)
}

// EditComment 编辑评论
func (c *CommentController) EditComment() {
	commentID := c.Ctx.Input.Param(":commentID")
	body := c.Ctx.Input.RequestBody
	var comment models.Comment
	if err := json.Unmarshal(body, &comment); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, err.Error(), "")
		return
	}
	if err := models.EditComment(c.DB, &comment, commentID, c.User.UserID); err != nil {
		models.LOGGER.Error("db error, reason: %v", err)
		c.FailJson(utils.CodeErrDb, err.Error(), "")
		return
	}
	c.OKJson(nil)
}

// DeleteComment 删除评论
func (c *CommentController) DeleteComment() {
	commentID := c.Ctx.Input.Param(":commentID")
	if err := models.DeleteComment(c.DB, commentID); err != nil {
		models.LOGGER.Error("DB error, reason: %v", err)
		c.ServeJSON()
		return
	}
	c.OKJson(nil)
}

// ShowComment 展示文章所有评论
func (c *CommentController) ShowComment() {
	articleID := c.Ctx.Input.Param(":articleID")

	shows, err := models.ShowComment(c.DB, articleID)
	if err != nil {
		models.LOGGER.Error("db error, reason: %v", err)
		c.ServeJSON()
		return
	}
	c.OKJson(map[string]interface{}{
		"comments": shows,
	})
}

// LikeComment 评论赞
func (c *CommentController) LikeComment() {
	commentID := c.Ctx.Input.Param(":contentID")
	if err := models.LikeComment(c.DB, commentID, c.User.UserID); err != nil {
		models.LOGGER.Error("DB error, reason: %v", err)
		c.FailJson(utils.CodeErrDb, err.Error(), "")
		return
	}
	c.OKJson(nil)
}

// ReplyComment 评论回复handler
func (c *CommentController) ReplyComment() {
	commentID := c.Ctx.Input.Param(":commentID")
	body := c.Ctx.Input.RequestBody
	var reply models.CommentReply
	if err := json.Unmarshal(body, &reply); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.ServeJSON()
		return
	}
	reply.CreatedBy = c.User.UserID
	if err := models.ReplyComment(c.DB, &reply, commentID); err != nil {
		models.LOGGER.Error("ReplyComment error, reason: %v", err)
		c.ServeJSON()
		return
	}
	c.OKJson(nil)
}
