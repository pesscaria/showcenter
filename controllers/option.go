package controllers

import (
	"showcenter/models"
	"utils"
)

//OptionController 操作路由
type OptionController struct {
	BaseController
}

// Like 赞
func (c *OptionController) Like() {
	articleID := c.Ctx.Input.Param(":articleID")
	models.LOGGER.Debug("articleID : [%s]", articleID)
	if err := models.Like(c.DB, articleID, c.User.UserID); err != nil {
		models.LOGGER.Error("Like error, reason :%v", err)
		c.Data["json"] = utils.FailReply(utils.CodeErrDb, "数据库错误", "文章不存在或则数据库错误")
		c.ServeJSON()
		return
	}
	c.OKJson(nil)
	return
}

// UnLike 取消赞
func (c *OptionController) UnLike() {
	articleID := c.Ctx.Input.Param(":articleID")
	if err := models.UnLike(c.DB, articleID, c.User.UserID); err != nil {
		models.LOGGER.Error("UnLike error, reason:%v", err)
		c.FailJson(utils.CodeErrDb, utils.DescErrDb, "未赞过该文章或者数据库错误")
		return
	}
	c.OKJson(nil)
	return
}

// Share 分享
func (c *OptionController) Share() {
	articleID := c.Ctx.Input.Param(":articleID")
	models.LOGGER.Debug("articleID : [%s]", articleID)
	if err := models.Share(c.DB, articleID, c.User.UserID); err != nil {
		models.LOGGER.Error("Like error, reason :%v", err)
		c.Data["json"] = utils.FailReply(utils.CodeErrDb, "数据库错误", "文章不存在或则数据库错误")
		c.ServeJSON()
		return
	}
	c.OKJson(nil)
	return
}

// Collect 收藏
func (c *OptionController) Collect() {
	articleID := c.Ctx.Input.Param(":articleID")
	models.LOGGER.Debug("articleID : [%s]", articleID)
	if err := models.Collect(c.DB, articleID, c.User.UserID); err != nil {
		models.LOGGER.Error("Like error, reason :%v", err)
		c.Data["json"] = utils.FailReply(utils.CodeErrDb, "数据库错误", "文章不存在或则数据库错误")
		c.ServeJSON()
		return
	}
	c.OKJson(nil)
	return
}

// UnCollect 取消收藏
func (c *OptionController) UnCollect() {
	articleID := c.Ctx.Input.Param(":articleID")
	if err := models.UnCollect(c.DB, articleID, c.User.UserID); err != nil {
		models.LOGGER.Error("UnLike error, reason:%v", err)
		c.FailJson(utils.CodeErrDb, utils.DescErrDb, "未收藏过该文章或者数据库错误")
		return
	}
	c.OKJson(nil)
	return
}

func (c *OptionController) Hit() {
	articleID := c.Ctx.Input.Param(":articleID")
	if err := models.Hit(c.DB, articleID, c.User.UserID); err != nil {
		models.LOGGER.Error("UnLike error, reason:%v", err)
		c.FailJson(utils.CodeErrDb, utils.DescErrDb, "数据库错误")
		return
	}
	c.OKJson(nil)
	return
}
