package controllers

import (
	"showcenter/models"
	"utils"
	"utils/proto/messagecenter"

	"encoding/json"
	"time"

	"golang.org/x/net/context"
)

// DatingController 文章相关处理Handler
type DatingController struct {
	BaseController
}

// NewDating 新建约会
func (c *DatingController) NewDating() {
	//判断权限
	//...
	var desc models.DatingDesc
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &desc); err != nil {
		models.LOGGER.Error("body unmarsharl error, reason:%v", err)
		c.FailJson(utils.CodeErrInvalidArgs, err.Error(), "")
		return
	}
	gid, err := models.NewArticle(c.DB, &desc, models.Location{}, c.User.UserID)
	if err != nil {
		models.LOGGER.Error("NewDating error, reason:%v", err)
		c.FailJson(utils.CodeErrDb, err.Error(), "")
		return
	}
	if _, err := utils.MessageClient.Group(
		context.Background(),
		&messagecenter.GroupReq{
			Opt:       utils.Create,
			GroupID:   gid,
			GroupName: desc.Title,
			Users:     []string{c.User.UserID},
		},
	); err != nil {
		models.LOGGER.Error("Group error, reason:%v", err)
		c.FailJson(utils.CodeErrNet, err.Error(), "")
		return
	}
	c.OKJson(nil)
}

//EditDating 编辑约会
func (c *DatingController) EditDating() {
	articleID := c.Ctx.Input.Param(":articleID")
	body := c.Ctx.Input.RequestBody
	var desc models.DatingDesc
	if err := json.Unmarshal(body, &desc); err != nil {
		models.LOGGER.Error("body unmarsharl error, reason:%v", err)
		c.FailJson(utils.CodeErrInvalidArgs, err.Error(), "")
		return
	}
	models.EditArticle(c.DB, &desc, articleID, c.User.UserID)
	c.OKJson(nil)
}

// DeleteDating 删除约会
func (c *DatingController) DeleteDating() {
	articleID := c.Ctx.Input.Param(":articleID")
	models.DeleteArticle(c.DB, articleID, c.User.UserID, c.User.IsSuperuser)
	c.OKJson(nil)
}

// ApplyJoinDating 申请加入约会
func (c *DatingController) ApplyJoinDating() {
	articleID := c.Ctx.Input.Param(":articleID")
	if err := models.ApplyDating(c.DB, articleID, c.User.UserID); err != nil {
		models.LOGGER.Error("ApplyDating error, reason: %v", err)
		c.FailJson(utils.CodeErrDb, utils.DescErrDb, err.Error())
		return
	}
	c.OKJson(nil)
}

// AgreeDating 同意加入申请
func (c *DatingController) AgreeDating() {
	var (
		req    map[string]string
		userid string
	)
	articleID := c.Ctx.Input.Param(":articleID")
	body := c.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &req); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, err.Error())
		return
	}
	if _, ok := req["userid"]; !ok {
		models.LOGGER.Error("Invilid param, userid is empty")
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, "userid is empty")
		return
	}
	if err := models.JoinDating(c.DB, articleID, userid, c.User.UserID); err != nil {
		models.LOGGER.Error("JoinDating error, reason: %v", err)
		c.FailJson(utils.CodeErrInternalServer, utils.DescErrInternalServer, err.Error())
		return
	}
}

/******************* 展示相关 *******************/

// HomePage 首页
func (c *DatingController) HomePage() {
	sinceID, _ := c.GetInt64("sinceid", 0)
	data := models.HomePage(c.DB, utils.Dating, sinceID)
	c.OKJson(data)
}

// ShowWithStyle 根据标签展示
func (c *DatingController) ShowWithStyle() {
	var tmp map[string][]string
	body := c.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &tmp); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, err.Error())
		return
	}
	index, ok := tmp["style"]
	if !ok {
		models.LOGGER.Error("Invilid param, style is empty")
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, "style is empty")
		return
	}
	querier := models.NewQuerier(c.DB, utils.Dating, models.IndexStyle, index)
	data := querier.QueryPage()
	c.OKJson(data)
}

// ShowWithDestination 根据目的地展示
func (c *DatingController) ShowWithDestination() {
	var tmp map[string][]string
	body := c.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &tmp); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, err.Error())
		return
	}
	index, ok := tmp["destination"]
	if !ok {
		models.LOGGER.Error("Invilid param, destination is empty")
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, "destination is empty")
		return
	}
	querier := models.NewQuerier(c.DB, utils.Dating, models.IndexDestination, index)
	data := querier.QueryPage()
	c.OKJson(data)
}

// ShowWithStartDate 根据出发日期展示
func (c *DatingController) ShowWithStartDate() {
	var tmp map[string]time.Time
	body := c.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &tmp); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, err.Error())
		return
	}
	index, ok := tmp["date"]
	if !ok {
		models.LOGGER.Error("Invilid param, date is empty")
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, "date is empty")
		return
	}
	querier := models.NewQuerier(c.DB, utils.Dating, models.IndexStartDate, index)
	data := querier.QueryPage()
	c.OKJson(data)
}
