package controllers

import (
	"encoding/json"
	"showcenter/models"
	"time"
	"utils"
)

// TourController 游玩handler
type TourController struct {
	BaseController
}

// NewTour 新建游玩
func (c *TourController) NewTour() {
	//判断权限
	//...
	var desc models.TourDesc
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &desc); err != nil {
		models.LOGGER.Error("body unmarsharl error, reason:%v", err)
		c.FailJson(utils.CodeErrInvalidArgs, err.Error(), "参数错误")
		return
	}
	_, err := models.NewArticle(c.DB, &desc, models.Location{}, c.User.UserID)
	if err != nil {
		models.LOGGER.Error("NewDating error, reason:%v", err)
		c.FailJson(utils.CodeErrDb, err.Error(), "")
		return
	}
	c.OKJson(nil)
}

// EditTour 编辑
func (c *TourController) EditTour() {
	articleID := c.Ctx.Input.Param(":articleID")
	body := c.Ctx.Input.RequestBody
	var desc models.TourDesc
	if err := json.Unmarshal(body, &desc); err != nil {
		models.LOGGER.Error("body unmarsharl error, reason:%v", err)
		c.FailJson(utils.CodeErrInvalidArgs, err.Error(), "")
		return
	}
	models.EditArticle(c.DB, &desc, articleID, c.User.UserID)
	c.OKJson(nil)
}

// DeleteTour 删除
func (c *TourController) DeleteTour() {
	articleID := c.Ctx.Input.Param(":articleID")
	models.DeleteArticle(c.DB, articleID, c.User.UserID, c.User.IsSuperuser)
	c.OKJson(nil)
}

/*
func (c *TourController) ShowTour() {

}
*/

//HomePage 首页
func (c *TourController) HomePage() {
	sinceID, _ := c.GetInt64("sinceid", 0)
	data := models.HomePage(c.DB, utils.Tour, sinceID)
	c.OKJson(data)
}

// ShowWithStyle 根据标签展示
func (c *TourController) ShowWithStyle() {
	var tmp map[string][]string
	body := c.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &tmp); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, err.Error())
		return
	}
	index, ok := tmp["style"]
	if !ok {
		models.LOGGER.Error("Invilid param, style is empty")
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, "style is empty")
		return
	}
	querier := models.NewQuerier(c.DB, utils.Tour, models.IndexStyle, index)
	data := querier.QueryPage()
	c.OKJson(data)
}

// ShowWithDestination 根据目的地展示
func (c *TourController) ShowWithDestination() {
	var tmp map[string][]string
	body := c.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &tmp); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, err.Error())
		return
	}
	index, ok := tmp["destination"]
	if !ok {
		models.LOGGER.Error("Invilid param, destination is empty")
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, "destination is empty")
		return
	}
	querier := models.NewQuerier(c.DB, utils.Tour, models.IndexDestination, index)
	data := querier.QueryPage()
	c.OKJson(data)
}

// ShowWithStartDate 根据出发日期展示
func (c *TourController) ShowWithStartDate() {
	var tmp map[string]time.Time
	body := c.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &tmp); err != nil {
		models.LOGGER.Error("Unmarshal error, reason: %v", err)
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, err.Error())
		return
	}
	index, ok := tmp["date"]
	if !ok {
		models.LOGGER.Error("Invilid param, date is empty")
		c.FailJson(utils.CodeErrInvalidArgs, utils.DescErrInvalidArgs, "date is empty")
		return
	}
	querier := models.NewQuerier(c.DB, utils.Tour, models.IndexStartDate, index)
	data := querier.QueryPage()
	c.OKJson(data)
}
