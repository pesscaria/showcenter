package controllers

import (
	"showcenter/models"
	"utils"
)

// PersonalController 个人信息业展示
type PersonalController struct {
	BaseController
}

// ListComments 展示个人参与的评论
func (c *PersonalController) ListComments() {

}

// ListCollections 展示个人收藏
func (c *PersonalController) ListCollections() {
	typ, err := c.GetInt("type")
	if err != nil {
		models.LOGGER.Error(utils.DescErrInvalidArgs)
		typ = utils.Dating
	}
	list, err := models.ListMyCollectedArticle(c.DB, c.User.UserID, typ)
	if err != nil {
		models.LOGGER.Error("%s: %v", utils.DescErrDb, err)
		c.FailJson(utils.CodeErrDb, utils.DescErrDb, "")
		return
	}
	c.OKJson(list)
}

// ListMine 展示个人发表文章
func (c *PersonalController) ListMine() {

}
