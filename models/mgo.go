package models

import (
	"log"

	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2"
)

type MongoBaseDao struct {
	*mgo.Session
}

var MongoPool *MongoBaseDao

func (pool *MongoBaseDao) Get() *mgo.Session {
	return pool.Session.Clone()
}

func init() {
	s, err := mgo.Dial(beego.AppConfig.String("mgo_addr"))
	if err != nil {
		log.Panic(err)
	}
	s.SetMode(mgo.Monotonic, true)
	s.SetPoolLimit(20)
	MongoPool = &MongoBaseDao{
		Session: s,
	}
	LOGGER.Debug("************* mongodb init **************")
}
