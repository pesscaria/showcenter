package models

import (
	"errors"
	"strings"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func CanLikeCommment(db *mgo.Database, commentID bson.ObjectId, userID string) bool {
	var r []CommentLikeRecord
	if err := db.C(COMMENTLIKES).Find(bson.M{"commentid": commentID, "createdby": userID}).All(&r); err != nil {
		LOGGER.Error("%v", err)
		return false
	}
	if len(r) != 0 {
		return false
	}
	return true
}

// NewCommnet 新建评论
func NewCommnet(db *mgo.Database, comment *Comment, articleID, userID string) error {
	var tmp map[string]interface{}
	c := db.C(CONTENTS)
	contentID := bson.ObjectIdHex(articleID)
	c.Find(bson.M{"_id": contentID}).One(&tmp)
	//tmp2 := tmp["content"].(map[string]interface{})
	//contentCreator := tmp2["createdby"].(string)
	c.Update(bson.M{"_id": contentID}, bson.M{"$inc": bson.M{"commentcount": 1}})
	id := bson.NewObjectId()
	now := time.Now()

	c = db.C(COMMENTS)
	comment.ID = id
	comment.ContentID = contentID
	comment.CreatedAt = now
	comment.CreatedBy = userID
	comment.UpdatedAt = now
	if err := c.Insert(&comment); err != nil {
		LOGGER.Error("Insert error,reason: %v", err)
		return err
	}
	return nil
}

// EditComment 编辑评论
func EditComment(db *mgo.Database, comment *Comment, commentID, userID string) error {
	var tmp Comment
	commentid := bson.ObjectIdHex(commentID)
	c := db.C(COMMENTS)
	if err := c.Find(bson.M{"_id": commentid}).One(&tmp); err != nil {
		LOGGER.Error("Find error, reason: %v", err)
		return err
	}
	if !strings.EqualFold(tmp.CreatedBy, userID) {
		LOGGER.Error("不是本人评论不能编辑")
		return errors.New("不是本人评论不能编辑")
	}
	if err := c.Update(bson.M{"_id": commentid}, bson.M{
		"content":   comment.Content,
		"updatedat": time.Now(),
		"updatedby": userID,
	}); err != nil {
		LOGGER.Error("Update error, reason: %v", err)
		return err
	}
	return nil
}

// DeleteComment 删除评论
func DeleteComment(db *mgo.Database, commentID string) error {
	var tmp Comment
	commentid := bson.ObjectIdHex(commentID)
	c := db.C(COMMENTS)
	if err := c.Find(bson.M{"_id": commentid}).One(&tmp); err != nil {
		LOGGER.Error("Find error, reason: %v", err)
		return err
	}
	if err := c.Remove(bson.M{"_id": commentid}); err != nil {
		LOGGER.Error("Remove error, reason: %v", err)
		return err
	}
	return nil
}

// ShowComment 展示评论
func ShowComment(db *mgo.Database, artcileID string) ([]CommentShow, error) {
	var (
		shows    []CommentShow
		comments []Comment
	)
	contentID := bson.ObjectIdHex(artcileID)
	c := db.C(COMMENTS)
	if err := c.Find(bson.M{"contentid": contentID}).Sort("-likecount").Sort("-createdat").All(&comments); err != nil {
		LOGGER.Error("Find error, reason: %v", err)
		return nil, err
	}
	c = db.C(REPLYS)
	for _, comment := range comments {
		var replys []CommentReply
		if err := c.Find(bson.M{"commentid": comment.ID}).Sort("-likecount").Sort("createat").All(&replys); err != nil {
			LOGGER.Error("find error, reason: %v", err)
			return nil, err
		}
		show := CommentShow{
			Comment: comment,
			Reply:   replys,
		}
		shows = append(shows, show)
	}
	return shows, nil
}

func LikeComment(db *mgo.Database, commentID, userID string) error {
	commentid := bson.ObjectIdHex(commentID)

	/*
		if err := db.C(COMMENTS).Find(bson.M{"_id": commentid}).One(&tmp); err != nil {
			LOGGER.Error("Find error, reason: %v", err)
			return err
		}
	*/
	count, err := db.C(COMMENTLIKES).Find(bson.M{
		"commentid": commentid,
		"createdby": userID,
	}).Count()
	if err != nil {
		LOGGER.Error("%v", err)
		return err
	}
	if count != 0 {
		LOGGER.Error("已经赞过此评论")
		return errors.New("已经赞过此评论")
	}
	id := bson.NewObjectId()
	r := &CommentLikeRecord{
		ID:        id,
		CommentID: commentid,
		UserID:    userID,
	}
	db.C(COMMENTLIKES).Insert(&r)
	db.C(COMMENTS).Update(bson.M{"_id": commentid}, bson.M{"$inc": bson.M{"likecount": 1}})
	return nil
}

func ReplyComment(db *mgo.Database, reply *CommentReply, commentID string) error {
	commentid := bson.ObjectIdHex(commentID)
	var tmp Comment
	if err := db.C(COMMENTS).Find(bson.M{"_id": commentid}).One(&tmp); err != nil {
		LOGGER.Error("Find error, reason: %v", err)
		return err
	}
	replyID := bson.NewObjectId()
	now := time.Now()
	reply.ID = replyID
	reply.CreatedAt = now
	db.C(REPLYS).Insert(&reply)
	db.C(CONTENTS).Update(bson.M{"_id": tmp.ContentID}, bson.M{"$inc": bson.M{"commentcount": 1}})
	return nil
}
