package models

import (
	"utils"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ListMyCollectedArticle 获取收藏的文章
func ListMyCollectedArticle(db *mgo.Database, userID string, typ int) (interface{}, error) {
	var (
		idm []map[string]bson.ObjectId
		ids []bson.ObjectId
	)
	LOGGER.Debug("%s %d", userID, typ)
	if err := db.C(COLLECTS).Find(bson.M{"userid": userID, "articletype": typ}).Select(bson.M{
		"_id":       0,
		"contentid": 1,
	}).Sort("-collectat").All(&idm); err != nil {
		LOGGER.Error("%v", err)
	}
	for _, v := range idm {
		if id, ok := v["contentid"]; ok {
			ids = append(ids, id)
		}
	}
	LOGGER.Debug("%v", ids)
	switch typ {
	case utils.Dating:
		var list []Dating
		db.C(CONTENTS).Find(bson.M{"_id": bson.M{"$in": ids}, "content.type": typ}).All(&list)
		return list, nil
	case utils.Tour:
		var list []Tour
		db.C(CONTENTS).Find(bson.M{"_id": bson.M{"$in": ids}, "content.type": typ}).All(&list)
		return list, nil
	default:
		LOGGER.Debug("Unknow artciletype")
	}
	return nil, nil
}

// ListMyCommentedArticle  获取评论过的文章
func ListMyCommentedArticle(db *mgo.Database, userID string, typ int) (interface{}, error) {
	return nil, nil
}

// ListMine 获取自己发布的文章
func ListMine(db *mgo.Database, userID string, typ int) (interface{}, error) {
	return nil, nil
}
