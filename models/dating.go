package models

import (
	"utils"

	"errors"
	"sync"
	"sync/atomic"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type datings struct {
	hub   map[string]*DatingStatu
	mutex sync.RWMutex
}

// DatingHub 仓库
var DatingHub datings

func (d *datings) getDating(datingID string) *DatingStatu {
	d.mutex.RLock()
	defer d.mutex.RUnlock()
	if v, ok := d.hub[datingID]; ok {
		return v
	}
	return nil

}

func loadDatingStatu() *datings {
	d := &datings{
		hub: make(map[string]*DatingStatu),
	}
	return d
}

func (d *datings) addDating(dating *DatingStatu) bool {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	if _, ok := d.hub[dating.datingID]; ok {
		return false
	}
	d.hub[dating.datingID] = dating
	return true
}

func (d *datings) delDating(datingID string) {
	d.mutex.Lock()
	delete(d.hub, datingID)
	d.mutex.Unlock()
}

//DatingStatu 某个约会的状态
type DatingStatu struct {
	datingID  string
	maxNumber int32
	curNumber int32
}

func newDatingStatu(datingID string, maxNumber, curNumber int32) *DatingStatu {
	return &DatingStatu{
		datingID:  datingID,
		maxNumber: maxNumber,
		curNumber: curNumber,
	}
}

//canJoin 是否可加入
func (d *DatingStatu) canJoin() bool {
	return atomic.LoadInt32(&d.curNumber) < d.maxNumber
}

// Join 加入约会
func (d *DatingStatu) Join() bool {
	if d.canJoin() {
		atomic.AddInt32(&d.curNumber, 1)
		return true
	}
	return false
}

// Exit 离开约会
func (d *DatingStatu) Exit() bool {
	atomic.AddInt32(&d.curNumber, -1)
	return true
}

// ApplyDating 申请加入约会
func ApplyDating(db *mgo.Database, articleID, userID string) error {
	var tmp map[string]string
	contentID := bson.ObjectIdHex(articleID)
	if err := db.C(CONTENTS).Find(bson.M{"_id": contentID}).Select(bson.M{
		"_id":               0,
		"content.createdby": 1,
	}).One(&tmp); err != nil {
		LOGGER.Error("Find error, reason: %v", err)
		return err
	}
	if creater, ok := tmp["createdby"]; ok {
		utils.PubEvent(
			utils.Article,
			utils.DatingApply,
			utils.ShowCenter,
			map[string]string{
				"manager":   creater,
				"applicant": userID,
				"datingID":  articleID,
			},
		)
	}
	return nil
}

// JoinDating 加入约会
func JoinDating(db *mgo.Database, articleID, joinUserID, leader string) error {
	dating := DatingHub.getDating(articleID)
	if dating == nil {
		LOGGER.Error("dating not exist")
		return errors.New("dating not exist")
	}
	if dating.Join() {
		contentID := bson.ObjectIdHex(articleID)
		if err := db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$inc": bson.M{"desc.curpartnernumber": 1, "$addToSet": bson.M{"desc.partner": joinUserID}}}); err != nil {
			LOGGER.Error("Update error, reason: %v", err)
			dating.Exit()
			return err
		}
	} else {
		LOGGER.Error("dating full")
		return errors.New("dating full")
	}
	return nil
}
