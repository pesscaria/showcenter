package models

import "gopkg.in/mgo.v2/bson"

type StoreArticleHandFunc func(body []byte) error

var Nodes map[string]*Node

// Node 文章分类
type Node struct {
	ID          bson.ObjectId `json:"id"`
	NodeID      string        `json:"nodeID"`
	Name        string        `json:"name"`
	Description string        `json:"description"`
	TopicCount  int           `json:"topicCount"`
}

func init() {
	//读取所有nodes分类,并初始化各分类的函数
	//valid.SetFieldsRequiredByDefault(true)
}
