package models

import (
	"utils"

	"errors"
	"strings"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// SaveArticle 保存文章
func SaveArticle(db *mgo.Database, article interface{}) error {
	if err := db.C(CONTENTS).Insert(article); err != nil {
		LOGGER.Error("Insert error, reason: %v", err)
		return err
	}
	return nil
}

// NewArticle 新建文章
func NewArticle(db *mgo.Database, descdata interface{}, loc Location, userID string) (string, error) {
	now := time.Now()
	id := bson.NewObjectId()
	content := Content{
		ID:        id,
		Index:     now.UnixNano(),
		Type:      utils.Dating,
		Loc:       loc,
		CreatedAt: now,
		CreatedBy: userID,
	}
	switch desc := descdata.(type) {
	case *DatingDesc:
		content.Title = desc.Title
		desc.Partner = []string{userID}
		desc.GroupID = id.Hex()
		desc.GroupName = desc.Title
		desc.CurPartnerNumber++
		dating := Dating{
			Content: content,
			Desc:    *desc,
			ID:      id,
		}
		if err := SaveArticle(db, &dating); err != nil {
			return "", err
		}
	case *TourDesc:
		content.Title = desc.Title
		tour := Tour{
			Content: content,
			Desc:    *desc,
			ID:      id,
		}
		if err := SaveArticle(db, &tour); err != nil {
			return "", err
		}
	default:
		LOGGER.Error("Unknow article type")
		return "", errors.New("Unknow article type")
	}
	return id.Hex(), nil
}

// EditArticle 编辑文章
func EditArticle(db *mgo.Database, descdata interface{}, articleID, userID string) error {
	var tmp map[string]Content
	contentID := bson.ObjectIdHex(articleID)
	if err := db.C(CONTENTS).Find(bson.M{"_id": contentID}).Select(bson.M{
		"_id":     0,
		"content": 1,
	}).One(&tmp); err != nil {
		LOGGER.Error("Find error, reason:%v", err)
		return err
	}
	content, ok := tmp["content"]
	if !ok {
		LOGGER.Error("Article not found")
		return errors.New("Article not found")
	}
	if !strings.EqualFold(content.CreatedBy, userID) {
		LOGGER.Error("没有该权限")
		return errors.New("没有该权限")
	}
	now := time.Now()
	switch desc := descdata.(type) {
	case DatingDesc:
		if err := db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$set": bson.M{
			"content.title":     desc.Title,
			"content.updatedat": now,
			"content.updatedby": userID,
			"desc.title":        desc.Title,
			"desc.style":        desc.Style,
			"desc.startdate":    desc.StartDate,
			"desc.enddate":      desc.EndDate,
			"desc.startpoint":   desc.StartPoint,
			"desc.destination":  desc.Destination,
			"desc.description":  desc.Description,
			"desc.picture":      desc.Picture,
		}}); err != nil {
			LOGGER.Error("Update error, reason: %v", err)
			return err
		}
	case TourDesc:
		if err := db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$set": bson.M{
			"content.title":     desc.Title,
			"content.updatedat": now,
			"content.updatedby": userID,
			"desc.title":        desc.Title,
			"desc.style":        desc.Style,
			"desc.startdate":    desc.StartDate,
			"desc.startpoint":   desc.StartPoint,
			"desc.destination":  desc.Destination,
			"desc.description":  desc.Description,
			"desc.plans":        desc.Plans,
			"desc.budget":       desc.Budget,
		}}); err != nil {
			LOGGER.Error("Update error, reason: %v", err)
			return err
		}
	default:
		LOGGER.Error("Unknow article type ")
		return errors.New("Unknow article type ")
	}
	return nil
}

// DeleteArticle 删除文章
func DeleteArticle(db *mgo.Database, articleID, userID string, isSuper bool) error {
	contentID := bson.ObjectIdHex(articleID)
	if !isSuper {
		var tmp map[string]Content
		db.C(CONTENTS).Find(bson.M{"_id": contentID}).One(&tmp)
		content, ok := tmp["content"]
		if !ok {
			LOGGER.Error("article not found")
			return errors.New("article not found")
		}
		if !strings.EqualFold(content.CreatedBy, userID) {
			LOGGER.Error("权限不足")
			return errors.New("权限不足")
		}
	}
	db.C(CONTENTS).Remove(bson.M{"_id": contentID})
	return nil
}

// HomePage 文章首页
func HomePage(db *mgo.Database, where int, sinceID int64) interface{} {
	switch where {
	case utils.Dating:
		var (
			list  []Dating
			query *mgo.Query
		)
		if sinceID == 0 {
			query = db.C(CONTENTS).Find(bson.M{"content.type": utils.Dating}).Sort("-content.createdat").Limit(perpage)
		} else {
			query = db.C(CONTENTS).Find(bson.M{"content.type": utils.Dating, "index": bson.M{"$lt": sinceID}}).Sort("-content.createdat").Limit(perpage)
		}
		if err := query.All(&list); err != nil {
			LOGGER.Error("%v", err)
			return nil
		}
		return list
	case utils.Tour:
		var (
			list  []Tour
			query *mgo.Query
		)
		if sinceID == 0 {
			query = db.C(CONTENTS).Find(bson.M{"content.type": utils.Tour}).Sort("-content.createdat").Limit(perpage)
		} else {
			query = db.C(CONTENTS).Find(bson.M{"content.type": utils.Tour, "index": bson.M{"$lt": sinceID}}).Sort("-content.createdat").Limit(perpage)
		}
		if err := query.All(&list); err != nil {
			LOGGER.Error("%v", err)
			return nil
		}
		return list
	}
	return nil
}
