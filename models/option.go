package models

import (
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Like 赞
func Like(db *mgo.Database, articleID, userID string) error {
	var tmp map[string]interface{}
	contentID := bson.ObjectIdHex(articleID)
	if err := db.C(CONTENTS).Find(bson.M{"_id": contentID}).One(&tmp); err != nil {
		LOGGER.Error("%v", err)
		return err
	}
	tmp2 := tmp["content"].(map[string]interface{})
	type_ := tmp2["type"].(int)

	db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$inc": bson.M{"content.likecount": 1}})
	id := bson.NewObjectId()
	like := &LikeRecord{
		ID:          id,
		ContentID:   bson.ObjectIdHex(articleID),
		UserID:      userID,
		LikeAt:      time.Now(),
		ArticleType: type_,
	}
	db.C(LIKES).Insert(like)
	return nil
}

// UnLike 取消赞
func UnLike(db *mgo.Database, articleID, userID string) error {
	var tmp LikeRecord
	contentID := bson.ObjectIdHex(articleID)
	if err := db.C(LIKES).Find(bson.M{"contentid": contentID, "userid": userID}).One(&tmp); err != nil {
		LOGGER.Error("%v", err)
		return err
	}
	db.C(LIKES).Remove(bson.M{"contentid": contentID, "userid": userID})
	db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$inc": bson.M{"content.likecount": -1}})

	return nil
}

// Share 分享
func Share(db *mgo.Database, articleID, userID string) error {
	var tmp map[string]interface{}
	contentID := bson.ObjectIdHex(articleID)
	if err := db.C(CONTENTS).Find(bson.M{"_id": contentID}).One(&tmp); err != nil {
		LOGGER.Error("%v", err)
		return err
	}
	tmp2 := tmp["content"].(map[string]interface{})
	type_ := tmp2["type"].(int)

	db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$inc": bson.M{"content.sharecount": 1}})
	id := bson.NewObjectId()
	share := &ShareRecord{
		ID:          id,
		ContentID:   bson.ObjectIdHex(articleID),
		UserID:      userID,
		ShareAt:     time.Now(),
		ArticleType: type_,
	}
	db.C(SHARES).Insert(share)
	return nil
}

// Collect 收藏
func Collect(db *mgo.Database, articleID, userID string) error {
	var tmp map[string]interface{}
	contentID := bson.ObjectIdHex(articleID)
	if err := db.C(CONTENTS).Find(bson.M{"_id": contentID}).One(&tmp); err != nil {
		LOGGER.Error("%v", err)
		return err
	}
	tmp2 := tmp["content"].(map[string]interface{})
	type_ := tmp2["type"].(int)

	db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$inc": bson.M{"content.collectcount": 1}})
	id := bson.NewObjectId()
	collect := &CollectRecord{
		ID:          id,
		ContentID:   bson.ObjectIdHex(articleID),
		UserID:      userID,
		CollectAt:   time.Now(),
		ArticleType: type_,
	}
	db.C(COLLECTS).Insert(collect)
	return nil
}

// UnCollect 取消收藏
func UnCollect(db *mgo.Database, articleID, userID string) error {
	var tmp CollectRecord
	contentID := bson.ObjectIdHex(articleID)
	if err := db.C(COLLECTS).Find(bson.M{"contentid": contentID, "userid": userID}).One(&tmp); err != nil {
		LOGGER.Error("%v", err)
		return err
	}
	db.C(COLLECTS).Remove(bson.M{"contentid": contentID, "userid": userID})
	db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$inc": bson.M{"content.collectcount": -1}})

	return nil

}

// Hit 点击查看文章
func Hit(db *mgo.Database, articleID, userID string) error {
	contentID := bson.ObjectIdHex(articleID)
	db.C(CONTENTS).Update(bson.M{"_id": contentID}, bson.M{"$inc": bson.M{"content.hitcount": 1}})
	return nil
}
