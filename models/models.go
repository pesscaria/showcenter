package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// 集合
const (
	CONTENTS     = "contents"
	COMMENTS     = "comments"
	REPLYS       = "replys"
	LIKES        = "likes"
	COMMENTLIKES = "commentlikes"
	COLLECTS     = "collects"
	SHARES       = "shares"
	NODES        = "nodes"
)

const (
	perpage = 10
)

/************************* Content **************************/

// Content 所有文章通用的内容
type Content struct {
	ID           bson.ObjectId `json:"-"` //同外层ID
	Index        int64         `json:"index"`
	Type         int           `json:"type"`
	Title        string        `json:"title"`        //required
	HitCount     int           `json:"hitCount"`     //观看数量
	LikeCount    int           `json:"likeCount"`    //点赞数量
	ShareCount   int           `json:"shareCount"`   //分享数量
	CommentCount int           `json:"commentCount"` //评论数量
	CollectCount int           `json:"collectCount"` //收藏数
	Loc          Location      `json:"location"`
	CreatedAt    time.Time     `json:"createdAt"`
	CreatedBy    string        `json:"createdBy"`
	UpdatedAt    time.Time     `json:"updatedAt"`
	UpdatedBy    string        `json:"updatedBy"`
	AuditState   int           `json:"auditState"`
	CanEdit      bool          `json:"canEdit" bson:"-"`
	CanLike      bool          `json:"canLike" bson:"-"`
	CanDelete    bool          `json:"canDelete" bson:"-"`
	CanCollect   bool          `json:"canCollect" bson:"-"`
}

// Location 发表人位置信息
type Location struct {
	City string `json:"city"`
	Lat  string `json:"latitude"`
	Lon  string `json:"longitude"`
}

/************************* Dating ***************************/

// DatingDesc 描述
type DatingDesc struct {
	Title            string    `json:"title"`
	Style            []string  `json:"style"`       //required
	StartDate        time.Time `json:"startDate"`   //required
	EndDate          time.Time `json:"endDate"`     //required
	StartPoint       string    `json:"startPoint"`  //required
	Destination      string    `json:"destination"` //required
	Description      string    `json:"description"` //required
	Picture          []string  `json:"picture"`
	Partner          []string  `json:"partner"`
	MaxPartnerNumber int32     `json:"maxPartnerNumber"` //required
	CurPartnerNumber int32     `json:"curPartnerNumber"`
	IsOver           bool      `json:"isOver"`
	GroupID          string    `json:"-"`
	GroupName        string    `json:"-"`
}

//Dating  约主题
type Dating struct {
	Content
	Desc  DatingDesc    `json:"desc"`
	ID    bson.ObjectId `json:"articleID" bson:"_id"`
	IsTop bool          `json:"isTop"`
}

/************************* Tour **************************/

// Spot 景点
type Spot struct {
	Name        string `json:"spotname"`
	Description string `json:"description"`
	Picture     string `json:"picture"`
	Other       string `json:"other"`
	Location    string `json:"location"`
}

// Plan 游玩计划
type Plan struct {
	Spots []Spot `json:"spots"`
}

// TourDesc 描述
type TourDesc struct {
	Title       string   `json:"title"`
	Style       []string `json:"style"`
	StartDate   string   `json:"startDate"`
	StartPoint  string   `json:"startPoint"`
	Destination string   `json:"destination"`
	Description string   `json:"description"`
	Plans       []Plan   `json:"plans"`
	Budget      int      `json:"budget"` //预算
}

// Tour 游玩主题
type Tour struct {
	Content
	Desc  TourDesc      `json:"desc"`
	ID    bson.ObjectId `bson:"id_" json:"articleID"`
	IsTop bool          `json:"istop"`
}

/************************ Comment *************************/

//Comment 评论
type Comment struct {
	ID        bson.ObjectId `bson:"_id"`
	ContentID bson.ObjectId `json:"articleID"`
	Content   string        `json:"content"`
	LikeCount int           `json:"likcecount"`
	CreatedBy string        `json:"createdBy"`
	CreatedAt time.Time     `json:"createdAt"`
	UpdatedBy string        `json:"updatedBy"`
	UpdatedAt time.Time     `json:"updatedAt"`
	IsLike    bool          `json:"islike"`
	CanEdit   bool          `json:"canEdit" bson:"-"`
	CanLike   bool          `json:"canLike" bson:"-"`
	CanDelete bool          `json:"canDelelte" bson:"-"`
}

//CommentReply 回复
type CommentReply struct {
	ID        bson.ObjectId `bson:"_id"`
	CommentID bson.ObjectId `json:"commentID"`
	CommnetBy string        `json:"commentBy"`
	Content   string        `json:"content"`
	LikeCount int           `json:"likecount"`
	Target    string        `json:"target"` //被回复人
	CreatedBy string        `json:"createdBy"`
	CreatedAt time.Time     `json:"createdAt"`
	UpdatedBy string        `json:"updatedBy"`
	UpdatedAt time.Time     `json:"updatedAt"`
	IsLike    bool          `json:"islike"`
}

// CommentLikeRecord 评论点赞记录
type CommentLikeRecord struct {
	ID        bson.ObjectId `bson:"_id"`
	UserID    string
	CommentID bson.ObjectId
}

//CommentShow 评论展示
type CommentShow struct {
	Comment Comment        `json:"comment"`
	Reply   []CommentReply `json:"reply"`
}

/************************ Record ************************/

// LikeRecord 点赞记录
type LikeRecord struct {
	ID          bson.ObjectId `bson:"_id"`
	UserID      string
	ContentID   bson.ObjectId
	ArticleType int
	LikeAt      time.Time
}

//ShareRecord 分享记录
type ShareRecord struct {
	ID          bson.ObjectId `bson:"_id"`
	UserID      string
	ContentID   bson.ObjectId
	ArticleType int
	ShareAt     time.Time
}

// CollectRecord 收藏记录
type CollectRecord struct {
	ID          bson.ObjectId `bson:"_id"`
	UserID      string
	ContentID   bson.ObjectId
	ArticleType int
	CollectAt   time.Time
}
