package models

import (
	"utils"

	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// 索引类型
const (
	Null = iota
	IndexStyle
	IndexDestination
	IndexStartDate
	IndexLoc
	IndexMaster
)

// Querier 查询器
type Querier struct {
	db          *mgo.Database
	contentType int
	indexType   int
	index       interface{}
}

// NewQuerier 新建查询器
func NewQuerier(db *mgo.Database, contentType, indexType int, index interface{}) *Querier {
	return &Querier{
		db:          db,
		contentType: contentType,
		indexType:   indexType,
		index:       index,
	}
}

// QueryPage 查询页
func (querier *Querier) QueryPage() interface{} {
	switch querier.contentType {
	case utils.Dating:
		var (
			list  []Dating
			query interface{}
		)
		switch querier.indexType {
		case IndexDestination:
			query = bson.M{
				"content.type": utils.Dating,
				"desc.destination": bson.M{
					"$in": querier.index.([]string),
				},
			}
		case IndexStyle:
			query = bson.M{
				"content.type": utils.Dating,
				"desc.style": bson.M{
					"$in": querier.index.([]string),
				},
			}
		case IndexStartDate:
			query = bson.M{
				"content.type": utils.Dating,
				"desc.startdate": bson.M{
					"$eq": querier.index.(time.Time),
				},
			}
		}
		if err := querier.db.C(CONTENTS).Find(query).Sort("-content.createdat").Limit(perpage).All(&list); err != nil {
			LOGGER.Error("Find error, reason: %v", err)
			return err
		}
		return list
	case utils.Tour:
		var (
			list  []Tour
			query interface{}
		)
		switch querier.indexType {
		case IndexDestination:
			query = bson.M{
				"content.type": utils.Tour,
				"desc.destination": bson.M{
					"$in": querier.index.([]string),
				},
			}
		case IndexStyle:
			query = bson.M{
				"content.type": utils.Tour,
				"desc.style": bson.M{
					"$in": querier.index.([]string),
				},
			}
		case IndexStartDate:
			query = bson.M{
				"content.type": utils.Tour,
				"desc.startdate": bson.M{
					"$eq": querier.index.(time.Time),
				},
			}
		}
		if err := querier.db.C(CONTENTS).Find(query).Sort("-content.createdat").Limit(perpage).All(&list); err != nil {
			LOGGER.Error("Find error, reason: %v", err)
			return err
		}
		return list
	}
	return nil
}
